#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from unittest import TestCase, main
from unittest.mock import patch

from DeamonChecker import DeamonChecker


class DeamonCheckerTest(TestCase):

    @patch("time.sleep", side_effect=InterruptedError)
    def test_service_checker(self, mocked_sleep):
        deamonChecker = DeamonChecker()
        deamonChecker.SERVICE = 'ImpossibleServiceName'
        self.assertEqual(deamonChecker.check_service(), False)

if __name__ == "__main__":
    main()
