#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import datetime
import os
import smtplib
import subprocess
import time


class DeamonChecker(object):
    def __init__(self):
        """
        Initialization of Deamon

        """
        config = configparser.ConfigParser()
        config.read('config.ini')
        self.SERVICE = config['DEFAULT']['Service']
        self.CHECKTIME = config['DEFAULT']['CheckTime']
        self.ATTEMPTSTIME = config['DEFAULT']['StartTime']
        self.ATTEMPTS = config['DEFAULT']['Attempts']
        self.EMAIL = config['DEFAULT']['Email']

        try:
            while True:
                self.watcher()
        except InterruptedError:
            print('Infinite loop broken')

    def watcher(self):
        """
        Infinite loop, which check if service is alive

        """
        if not self.check_service():
            self.handle_stop()
        time.sleep(int(self.CHECKTIME))

    def check_service(self):
        """
        Check if service is online

        Returns:
            (boolean): True if service is alive

        """
        current_services = subprocess.check_output("service --status-all | grep +", shell=True)
        if str.encode(self.SERVICE) in current_services:
            return True
        return False

    def handle_stop(self):
        """
            Handle the stopped service

        """
        log = dict()
        log['status'] = ""
        for attempt in range(1, int(self.ATTEMPTS) + 1):
            log['attempt'] = attempt
            subprocess.call(["service", self.SERVICE, "start"])
            success = self.check_service()
            if success:
                log['status'] = True
                break
            time.sleep(int(self.ATTEMPTSTIME))
        self.log_stop(log)
        self.send_mail(log)

    def log_stop(self, log):
        """
        Log information about crash event in file

        Args:
            log (dict): dict with information about event

        """
        current_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        log_message = "{}: {} stopped. After {} attempt(s) of restart service is {}.\n" \
            .format(current_time, self.SERVICE, log['attempt'], 'up' if log['status'] else 'down')
        with open(os.path.abspath("/var/log/deamon.log"), 'a+') as file:
            file.write(log_message)

    def send_mail(self, log):
        """
        Send email about crash event using local SMTP
        Args:
            log (dict): dict with information about event

        """
        body = "Your service {} has stopped. Our script tried to restart it and after {} attempt(s) service is {}" \
            .format(self.SERVICE, log['attempt'], 'up' if log['status'] else 'down')
        try:
            server = smtplib.SMTP('localhost')
            server.sendmail('randomMail@sth.com', self.EMAIL, body)
        except:
            print("Error: unable to send email")

if __name__ == "__main__":
    deamonChecker = DeamonChecker()
